<?php

namespace App\Http\Controllers;


use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function index()
    {
        $roles  = Role::latest()->get();

         //make response JSON
         return response()->json([
            'success' => true,
            'message' => 'List Data Role',
            'data'    => $roles 
        ], 200);
    }
 
    
    public function store(Request $request)
    {
        $allRequest = $request->all();
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);  

         //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $role = Role::create([
            'name' => $request -> name,
            
        ]);
    
        //success save to database
            if($role) {
            return response()->json([
        'success' => true,
        'message' => 'Data Role berhasil dibuat',
        'data'    => $role  
        ], 200);
        } 
        
        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data Role gagal dibuat',
        ], 409);
    }

    public function show($id)
    {
        //find role by ID
        $role = Role::find($id);
        
       if ($role) {
    //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Role',
            'data'    => $role
        ], 200);
       } 

       //failed save to database
       return response()->json([
        'success' => false,
        'message' => 'Data Dengan Id : ' .  $id  . 'tidak ditemukan',
    ], 404);
 
    }

    public function update(Request $request, $id)
    {
       $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'name'   => 'required',
            
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $role = Role::find($id);

        if($role) {

            //update post
            $role->update([
                'name'     => $request->name,
                
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data berhasil di Update',
                'data'    => $role  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Role Tidak ditemukan',
        ], 404);

    }


    public function destroy($id)
    {
        //find post by ID
        $role = Role::find($id);

        if($role) {

            //delete post
            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role Berhasil di Hapus',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Data Dengan Id : ' . $id .'tidak ditemukan',
        ], 404);
    }
}
