<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\otpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use SebastianBergmann\Type\FalseType;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {   
        $allRequest  = $request->all();
        
        //set validation
        $validator = Validator::make($allRequest, [
            'otp' => 'required',
        ]);

         //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otp_code = otpCode::where ('otp' , $request->otp)->first();
        
        if (!$otp_code)
        {
            return response ()->json([
                'succes' => False,
                'message' => 'OTP Code tidak ditemukan'
            ], 400);
        }

        $now = Carbon::now();

        if( $now > $otp_code->valid_until)
        {
            return response ()->json([
                'succes' => false,
                'message' => 'OTP Code tidak berlaku lagi'
            ], 400);
        }

        $user = User::find($otp_code->user_id);
        $user->update([
            'email_verified_at' => $now
        ]);

        $otp_code->delete();

        return response ()->json([
            'succes' => true,
            'message' => 'User Berhasil diverifikasi',
            'data' => $user,
        ], 200);
    }
        
}
