<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 

class PostController extends Controller
{
    
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store' , 'update' , 'delete']);
    }
    
    public function index()
    {
        $post = Post::latest()->get();

         //make response JSON
         return response()->json([
            'success' => true,
            'message' => 'List Data Post',
            'data'    => $post  
        ], 200);
    }

    
    public function store(Request $request)
    {
        $allRequest  = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'title'   => 'required',
            'description' => 'required',
        ]);

         //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        

        //save to database
        $post = Post::create([
            'title' => $request -> title,
            'description' => $request -> description,
           
        ]);
    
        //success save to database
            if($post) {
            return response()->json([
        'success' => true,
        'message' => 'Data Post berhasil dibuat',
        'data'    => $post  
        ], 200);
        } 
        
        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data Post gagal dibuat',
        ], 409);
    } 

    public function show($id)
    {
        //find post by ID
        $post = Post::find($id);



        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $post
        ], 200);

         //failed save to database
       return response()->json([
        'success' => false,
        'message' => 'Data Dengan Id : ' .  $id  . 'tidak ditemukan',
    ], 404);

    }

    public function update(Request $request, $id)
    {
       $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $post = Post::find($id);

        if($post) {

            //update post
            $post->update([
                'title'     => $request->title,
                'description'   => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data berhasil di Update',
                'data'    => $post  
            ], 200);

        }
 
        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);

    }

    public function destroy($id)
    {
        //find post by ID
        $post = Post::find($id);

        if($post) {

            //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil di Hapus',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Tidak ditemukan',
        ], 404);
    }
}

 