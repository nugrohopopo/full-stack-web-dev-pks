<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $user = User::latest()->get();

         //make response JSON
         return response()->json([
            'success' => true,
            'message' => 'List Data User',
            'data'    => $user 
        ], 200);
    }

    
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'username'   => 'required',
            'email'   => 'required',
            'role_id'   => 'required',
            
        ]);

         //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $user = User::create([
            'name' => $request -> name,
            'username' => $request -> username,
            'email' => $request -> email,
            'role_id' => $request -> role_id,
            
        ]);
    
        //success save to database
            if($user) {
            return response()->json([
        'success' => true,
        'message' => 'Data User berhasil dibuat',
        'data'    => $user  
        ], 200);
        } 
        
        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data User gagal dibuat',
        ], 409);
    }

    public function show(Request $request)
    {
        //find User by ID
        $user = User::findOrfail($request->id);


        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data User',
            'data'    => $user
        ], 200);

    }

    public function update(Request $request, $id)
    {
       $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'name'   => 'required',
            'username'   => 'required',
            'email'   => 'required',
            'role_id'   => 'required',
            
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $user = User::find($id);

        if($user) {

            //update post'content' => $request -> content,
            $user->update([
            'name' => $request -> name,
            'username' => $request -> username,
            'email' => $request -> email,
            'role_id' => $request -> role_id,
                
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data berhasil di Update',
                'data'    => $user  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'User Tidak ditemukan',
        ], 404);

    }

    public function destroy($id)
    {
        //find post by ID
        $user = User::find($id);

        if($user) {

            //delete post
            $user->delete();

            return response()->json([
                'success' => true,
                'message' => 'User Berhasil di Hapus',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'User Tidak ditemukan',
        ], 404);
    }
}


