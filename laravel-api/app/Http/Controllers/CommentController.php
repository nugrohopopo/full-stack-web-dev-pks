<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\CommentStoredEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function index(Request $request)

    {
       $post_id = $request->post_id;
        $comment = Comment::where ('post_id' ,$post_id)->latest()->get();

         //make response JSON
         return response()->json([
            'success' => true,
            'message' => 'List Data Comment',
            'data'    => $comment   
        ], 200);
    }

    
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id'   => 'required',
            
        ]);

         //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comment = Comment::create([
            'content' => $request -> content,
            'post_id' => $request -> post_id,
        ]);

        //memanggil event CommentStoreEvent
        event(new CommentStoredEvent($comment));


    
        //success save to database
            if($comment) {
            return response()->json([
        'success' => true,
        'message' => 'Data Comment berhasil dibuat',
        'data'    => $comment  
        ], 200);
        } 
        
        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data Comment gagal dibuat',
        ], 409);
    }

    public function show($id)
    {
        //find Comment by ID
        $comment = Comment::find($id);


        //make response JSON
        return response()->json([ 
            'success' => true,
            'message' => 'Detail Data Comment',
            'data'    => $comment
        ], 200);

         //failed save to database
         return response()->json([
            'success' => false,
            'message' => 'Data Dengan Id : ' .  $id  . 'tidak ditemukan',
        ], 404);
 
    }

    public function update(Request $request, $id)
    {
       $allRequest = $request->all();
        //set validation
        $validator = Validator::make($allRequest, [
            'content'   => 'required',
            
            
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $comment = Comment::find($id);

        if($comment) {

            //update post
            $comment->update([
                'content'     => $request->content,
                
                
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data Comment berhasil di Update',
                'data'    => $comment  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Data Dengan Id : ' . $id .'tidak ditemukan',
        ], 404);

    }

    public function destroy($id)
    {
        //find post by ID
        $comment = Comment::find($id);

        if($comment) {

            //delete post
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data Comment Berhasil di Hapus',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Data Dengan Id : ' . $id .'tidak ditemukan',
        ], 404);
    }
}



