<?php

namespace App\Listeners;

use App\Events\OtpNewEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailNewOtpToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpNewEvent  $event
     * @return void
     */
    public function handle(OtpNewEvent $event)
    {
        //
    }
}
