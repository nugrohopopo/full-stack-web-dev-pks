<?php

namespace App\Listeners;

use App\Events\RegisterStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterStoredEvent  $event
     * @return void
     */
    public function handle(RegisterStoredEvent $event)
    {
        Mail::to($event->comment->post->user->email)->send(new PostAuthorMail($event->comment));
    }
}
