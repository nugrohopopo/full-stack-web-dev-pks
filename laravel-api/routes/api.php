<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Table Post
// Route::get('/post', 'PostController@index');
// Route::post('/post', 'PostController@store');
// Route::get('/post/{id}', 'PostController@show');
// Route::put('/post/{id}', 'PostController@update');
// Route::delete('/post/{id}', 'PostController@destroy');

// //Table Role
// Route::get('/role', 'RoleController@index');
// Route::post('/role', 'RoleController@store');
// Route::get('/role/{id}', 'RoleController@show');
// Route::put('/role/{id}', 'RoleController@update');
// Route::delete('/role/{id}', 'RoleController@destroy');

// //Table User
// Route::get('/user', 'UserController@index');
// Route::post('/user', 'UserController@store');
// Route::get('/user/{id}', 'UserController@show');
// Route::put('/user/{id}', 'UserController@update');
// Route::delete('/user/{id}', 'UserController@destroy');

// //Table Comment
// Route::get('/comment', 'CommentController@index');
// Route::post('/comment', 'CommentController@store');
// Route::get('/comment/{id}', 'CommentController@show');
// Route::put('/comment/{id}', 'RoleController@update');
// Route::delete('/comment/{id}', 'CommentController@destroy');

Route::apiResource('post', 'PostController');
Route::apiResource('comment', 'CommentController');
Route::apiResource('role', 'RoleController');

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
] , function(){
    
    Route::post('register' , 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code' , 'RegenerateOtpCodeController')->name('auth.regenerate_otp_code');
    Route::post('verification' , 'VerificationController')->name('auth.verification');
    Route::post('update-password' , 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login' , 'LoginController')->name('auth.login');
});

Route::get('/test', function(){
return 'masuk pak EKO';
}) -> middleware('auth:api');