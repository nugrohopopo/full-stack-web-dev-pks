
// Soal Nomer 1
const  kelilingPersegipanjang = (panjang, lebar)=>{
    return (panjang+lebar)*2
};
console.log('Keliling Persegi Panjang = ',  kelilingPersegipanjang(10,5));


let  luasPersegipanjang = function(p,l)
{
	return p*l;
};
console.log('Luas Persegi Panjang =',luasPersegipanjang(10,5));


// Soal Nomer 2
  const fullName = (firstName, lastName)=>{
    return (`${firstName} ${lastName}`)
  }
  
  console.log(fullName("William", "Imoh"));


//Soal Nomor 3
  const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
  const {firstName, lastName, address, hobby} = newObject

  console.log(firstName, lastName, address, hobby)


//Soal Nomor 4 
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

let combinedArray = [...west, ...east]

console.log(combinedArray)


//Soal Nomor 5
const planet = "earth" 
const view = "glass" 
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} `

console.log(before)